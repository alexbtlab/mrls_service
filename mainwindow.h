#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>
#include <QTimer>
#include <QLineSeries>
#include <QChartView>
#include <QTimer>
#include <QLineSeries>
#include <QLCDNumber>
#include <QValueAxis>
#include <QProcess>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum tStateView{
 ALL_DATA ,
 DIFF_DATA,
 RAW_DATA
};

void stateView (tStateView stateView);

void drawAzimuth(uint32_t * pRawData, uint32_t * pRawDataSub);
uint32_t converReceivedDataToU32(char *datagrama, size_t ind);

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void ItemDebug();
private slots:

    void on_pushButton_25_clicked();
    void slotTimerAlarm();
    void on_pushButton_11_clicked();
    void on_button_singleAzimuthView_clicked();
    void on_pushButton_allDataView_clicked();
    void on_pushButton_SetXY_clicked();
//    void on_pushButton_27_clicked();
//    void on_pushButton_29_clicked();
    void on_pushButton_RawDataView_clicked();
    void on_pushButton_ClearChar_clicked();
    void on_pushButton_DiffDataView_clicked();
    void on_pushButton_StartBGCollecting_clicked();
    void on_pushButton_AllAzimutView_clicked();
    void on_pushButton_RawData_clicked();
    void on_pushButton_19_clicked();
    void on_pushButton_SendHB_clicked();
    void on_pushButton_EnPamp_clicked();
    void on_pushButton_atten_10_clicked();
    void on_pushButton_atten_20_clicked();
    void on_pushButton_atten_30_clicked();
    void on_pushButton_atten_40_clicked();
    void on_pushButton_atten_50_clicked();
    void on_pushButton_atten_60_clicked();
    void on_pushButton_rxamp_1_clicked();
    void on_pushButton_rxamp_2_clicked();
    void on_pushButton_rxamp_3_clicked();
    void on_pushButton_rxamp_4_clicked();
    void on_pushButton_atten_0_clicked();
    void on_pushButton_DisPamp_clicked();
    void on_button_OneAzimuth_clicked();
    void on_button_threshhold_clicked();

    void on_button_numAzimuth_clicked();

    void on_pushButton_XYFull_clicked();

    void on_pushButton_XYLeftSide_clicked();

    void on_pushButton_XYRigthSide_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_AttenSet_clicked();

    void on_button_ShiftValueQM_clicked();

//    void on_pushButton_GetCompassAzimuth_clicked();

    void on_lineEdit_IP_MavLink_cursorPositionChanged(int arg1, int arg2);

    void on_pushButton_RunMotor_clicked();

    void on_pushButton_StopMotor_clicked();

    void on_button_shiftDCO_clicked();


    void on_pushButton_GetTelemetryMRLS2D_clicked();

    void on_pushButton_Set120MHz_HMC769_clicked();

    void on_pushButton_MotorSpeed1_25sec_clicked();

    void on_pushButton_MotorSpeed2_5sec_clicked();

    void on_pushButton_MotorSpeed5sec_clicked();


    void on_pushButton_SetCustomFreqBand_clicked();

    void on_pushButton_SetAtten_clicked();

    void on_pushButton_EnablePrintTelemetry_clicked();

    void on_pushButton_TrigerSwitch_clicked();

//    void on_value_Fstart_textChanged(const QString &arg1);

    void on_button_ShiftDCO_clicked();

    void on_pushButton_AGCEnable_clicked();

private:
    Ui::MainWindow *ui;
    QUdpSocket *mSocket = nullptr;
    QUdpSocket *mSocketMavLink = nullptr;
    QTimer *timer;
    QMainWindow *nw ;
    void readPendingDatagrams();

    void readPendingDatagramsMavLink();

    QChartView *chartView;

    static QLCDNumber * lcdNumber;
//    QValueAxis *xAxis;
//    QValueAxis *yAxis;

};
#endif // MAINWINDOW_H
