#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QChart>
#include <QChartView>
#include <QLineSeries>
#include <QValueAxis>
#include <QLogValueAxis>
#include <QLineSeries>
#include <QValueAxis>
#include <QChart>
#include <QChartView>
#include <QUdpSocket>
#include <math.h>
#include <QTimer>
#include <QNetworkDatagram>
#include <QPolarChart>
#include <QScatterSeries>
#include <QValueAxis>
#include <QByteArray>
#include <QProcess>
#include <QProcess>
#include <QListWidget>
#include <csignal>
#include <cstring>
#include "mavlink/mavlink_parser/mavlink_parser.h"
#include "mavlink/mavlink_parser/parser.c"
#include "mavlink_parser.h"
#include <string.h>
#include <stdbool.h>

#define TARGET_SYSTEM    55
#define TARGET_COMPONENT 0
#define SYS_ID 55
#define COMPONENT_ID 0
#define ADDITEM_DEBUG(str)  ui->listWidget->addItem(str); \
                            ui->listWidget->scrollToBottom();  // Добавляем строку в отладочную консоль
#define MRLS_VERSION 2


QProcess process;
QLineSeries seriesRawData;          // Серия точек для отображения "Сырых данных"
QLineSeries seriesRawDataSub;       // Серия точек для отображения "Разницы между RawData и собранным фоном"
QValueAxis xAxis;
QValueAxis yAxis;

static size_t testAzimuth = 0;
bool enableCount = false;
bool needed_send_param = false;
float tempFPGA;
float tempLDO;
float tempADPart;

extern bool enable_transmit_RAWdata_to_PC;
extern bool GPSValueIsRead;
extern uint32_t compassValueAzimuth;
extern bool CompassValueIsRead;
extern uint64_t GPSCoordinate;

static volatile bool background_is_collected = false;           // Флаг - "Фон собран"
static volatile bool enableStartBackGrounCollection = false;    // Флаг - "Разрешение сбора фона"
static volatile bool replayStart =  false;                      // Флаг - "Воспроизвести собранный фон"
static volatile bool oneAzimuthView = false;                    // Флаг - "Отображение одного азимута"
static volatile bool backGroundNedeedClear = false;             // Флаг - "Необходимо очистить фон" (для сбора нового фона)
static volatile bool diffDataView = false;                      // Флаг состояния отображения данных на графике "Отображение разницы между RAW и ФОНОМ"
static volatile bool allDataView = false;                       // Флаг состояния отображения данных на графике "Отображение ВСЕХ данных"
static volatile bool RawDataVievIsActive = true;                // Флаг состояния отображения данных на графике "Отображение сырых данных"

static size_t sizeReadUdpData = 2048;                             // Размер принимаемых пакетов по UDP
static const size_t maxNumAzimuth = 10000;                                // Максимальное значение количества азимутов
static int16_t currentAzimuth = 0;                                     // Текущее значение азимута
static uint64_t rotate = 0;                                             // Текущее количество оборотов
static uint32_t num_rotate = 10;                                        // Количество оборотов для сбора фона
static int32_t rangeAzimuthViewValue = 0;                              // Диапазон азимутов для отображения диапазона азимутов
static uint32_t maxAzimuthViewValue = 0;                                // Максимальное значение азимута для отображения диапазона азимутов
static uint32_t minAzimuthViewValue = 0;                                // Минимальное значение азимута для отображения диапазона азимутов
static volatile uint32_t backGround[maxNumAzimuth][8192];    // Массив для хранения фона
static uint32_t cnt = 0;
static uint32_t threshold = 0;
struct tcp_pcb *server_pcb;


int handler_PARAM_VALUE(mavlink_message_t* message);
void handler(mavlink_message_t* message);
static int16_t readAzimuth(char *datagram);

static int16_t readAzimuth(char *datagram){

    if (  *datagram      == 'R' &&
          *(datagram+1)  == 'A' &&
          *(datagram+2)  == 'W' &&
          *(datagram+3)  == 'E' )   {
                                        if ( currentAzimuth == 10){
                                            if( enableStartBackGrounCollection )  rotate++;
                                            else                                  rotate = 0;
                                        }



                                        return currentAzimuth = (   (uint16_t)( (*(datagram + 11) << 8) | (uint8_t)(*(datagram + 10))   )  );
                                    }
    return -1;
}
void checkAzimuth(){

    if ( currentAzimuth == 1){
        enableCount =  true;
        testAzimuth = 1;
    }

    if( enableCount ){
        if(testAzimuth != currentAzimuth) {
            qDebug() << "Err" << testAzimuth << currentAzimuth; while(1);
        }
            testAzimuth++;
            if ( testAzimuth == 284)
                testAzimuth = 0;
    }
}

void checkAzimuthMaxAzimuth( int32_t currentAzimuth ){

    static int32_t lastAzimuth = 0;

    if( currentAzimuth ==  0 && lastAzimuth != 0 ){
        if( lastAzimuth != 283  )
             qDebug() << "Azimuth amount is nor equal 283...last" << lastAzimuth;
    }
    lastAzimuth = currentAzimuth;
}

uint32_t converReceivedDataToU32(char *datagrama, size_t ind){

    uint32_t resData;
    return                                        resData = (uint32_t)   (   (( (uint8_t)*( datagrama + 0 + (ind*4) )  << 0   ))  |
                                                                             (( (uint8_t)*( datagrama + 1 + (ind*4) )  << 8   ))  |
                                                                             (( (uint8_t)*( datagrama + 2 + (ind*4) )  << 16  ))  |
                                                                             (( (uint8_t)*( datagrama + 3 + (ind*4) )  << 24  ))  );   // read 32'b word FFT[8192]
}
void stateView (tStateView stateView){
    if (stateView == ALL_DATA){
        allDataView = true;
        RawDataVievIsActive = false;
        diffDataView        = false;
    }
    if (stateView == DIFF_DATA){
        allDataView = false;
        RawDataVievIsActive = false;
        diffDataView        = true;
    }
    if (stateView == RAW_DATA){
        allDataView = false;
        RawDataVievIsActive = true;
        diffDataView        = false;
    }
}
void drawAzimuth(uint32_t * pRawData, uint32_t * pRawDataSub){

    if( background_is_collected ){

        if( allDataView ){
            seriesRawData.clear();
            seriesRawDataSub.clear();
            for (uint32_t j = 0; j < sizeReadUdpData;  j++)
                 { seriesRawData.append(j,    *(pRawData+j) ); seriesRawDataSub.append(j,  pRawDataSub[j] );}

        }
        if( RawDataVievIsActive ){
            seriesRawData.clear();
            for (uint32_t j = 0; j < sizeReadUdpData;  j++)
                 seriesRawData.append(j,    *(pRawData+j) );

        }
        if( diffDataView ){
            seriesRawDataSub.clear();
            for (uint32_t j = 0; j < sizeReadUdpData;  j++)
                seriesRawDataSub.append(j,  pRawDataSub[j] );
        }
    }
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowIcon(QIcon("/home/alexbtlab/source/mrls_service/index.png"));

    mSocket = new QUdpSocket(this);
    connect(mSocket, &QUdpSocket::readyRead,
            this, &MainWindow::readPendingDatagrams);

    mSocket->bind(ui->port_RawData->value(), QUdpSocket::ShareAddress);

    mSocketMavLink = new QUdpSocket(this);
    connect(mSocketMavLink, &QUdpSocket::readyRead,
            this, &MainWindow::readPendingDatagramsMavLink);

    mSocketMavLink->bind(ui->port_mavlink->value(), QUdpSocket::ShareAddress);

    QString strDebugOutLW = "INFO: waiting for RawData packet from port:" + ui->port_RawData->text() +
                            " mavLinkControl port:" + ui->port_mavlink->text();
    ADDITEM_DEBUG(strDebugOutLW)

    ui->progressBar->setOrientation(Qt::Horizontal);
        ui->progressBar->setRange(0, 100); // Let's say it goes from 0 to 100
        ui->progressBar->setValue(0); // With a current value of 10

    QMainWindow *nw = new QMainWindow();

    QPen penRawData(QRgb(0xababab));
        penRawData.setWidth(1);
    QPen penRawDataSub(QRgb(0xff0000));
        penRawDataSub.setWidth(1);

        seriesRawData.setPen(penRawData);
        seriesRawData.setUseOpenGL(true);
        seriesRawData.append(0, 750000);
        seriesRawData.append(0, 0);
        seriesRawData.append(sizeReadUdpData, 0);
        seriesRawData.append(0, 0);

        seriesRawDataSub.setPen(penRawDataSub);
        seriesRawDataSub.setUseOpenGL(true);
        seriesRawDataSub.append(0, 7500000);
        seriesRawDataSub.append(0, 0);
        seriesRawDataSub.append(sizeReadUdpData, 0);
        seriesRawDataSub.append(0, 0);

        xAxis.setRange(0, sizeReadUdpData);
        yAxis.setRange(0, 2500000);

    QChart *charRaw = new QChart();
        charRaw->legend()->hide();
        charRaw->addSeries(&seriesRawData);
        charRaw->addSeries(&seriesRawDataSub);
        charRaw->createDefaultAxes();
        charRaw->setTitle("RawData");

    QChartView *chartView = new QChartView(charRaw);
        chartView->setRenderHint(QPainter::Antialiasing);
        chartView->chart()->setAxisX(&xAxis, &seriesRawData);
        chartView->chart()->setAxisY(&yAxis, &seriesRawData);
        chartView->chart()->setAxisX(&xAxis, &seriesRawDataSub);
        chartView->chart()->setAxisY(&yAxis, &seriesRawDataSub);

        nw->setCentralWidget(chartView);
        nw->resize(800, 600);
        nw->show();

    QScatterSeries *seriesScatter = new QScatterSeries();
        seriesScatter->setUseOpenGL(true);

    QLineSeries *series1 = new QLineSeries();
        series1->append(0, sizeReadUdpData);
        series1->append(0, 0);
        series1->append(45, sizeReadUdpData);
        series1->append(45, 0);
        series1->append(90, sizeReadUdpData);

    seriesScatter->setMarkerSize(15);
        for (int i = 0; i < 360; i += 10)
            seriesScatter->append(100, i);

    QLineSeries *series2 = new QLineSeries();
        series2->append(0, 0);
        series2->append(90, 0);
        series2->append(180, 0);
        series2->append(270, 0);
        series2->append(360, 0);

    QValueAxis *angularAxis = new QValueAxis();
        angularAxis->setRange(0, 360);

    QPolarChart *chartPol = new QPolarChart();
        chartPol->addSeries(series1);
        chartPol->addSeries(series2);
        chartPol->addSeries(seriesScatter);
        chartPol->addAxis(angularAxis, QPolarChart::PolarOrientationAngular);

    QValueAxis *radialAxis = new QValueAxis();
        radialAxis->setTickCount(9);
        radialAxis->setLabelFormat("%d");
        chartPol->addAxis(radialAxis, QPolarChart::PolarOrientationRadial);

        series1->attachAxis(radialAxis);
        series1->attachAxis(angularAxis);
        series2->attachAxis(radialAxis);
        series2->attachAxis(angularAxis);

    QChartView *chartViewPol = new QChartView(chartPol);
    chartViewPol->setChart(chartPol);
    chartViewPol->setRenderHint(QPainter::Antialiasing);

//    ui->graphicsView->setChart(chartPol);

    background_is_collected = true;     // Включаем признак "ФОН СОБРАН". При установленном признаке
    stateView ( RAW_DATA );
}
MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::readPendingDatagrams(){

        if(mSocket->hasPendingDatagrams()){
            QByteArray datagrama;

            datagrama.resize(mSocket->pendingDatagramSize());
//           quint16 remotePort;

//           QHostAddress remoteAdr;
//           size_t sizeDatagram = datagrama.size();
            mSocket->readDatagram( datagrama.data(), datagrama.size());
//            qDebug() << "remPort" << remotePort;
//            qDebug() << "remAdr" << (remoteAdr.toIPv4Address());
            uint32_t rawData[8192] = {0,};
            uint32_t rawDataDiff[8192]    = {0,};
            currentAzimuth = readAzimuth(datagrama.data());        // Чтение из принятой датагаммы АЗИМУТА
            if ( currentAzimuth == -1 )
                return;

//            checkAzimuthMaxAzimuth( currentAzimuth );
//            qDebug() << currentAzimuth;
//            checkAzimuth();

            for (uint32_t i = 0; i < sizeReadUdpData;  i++)
                   rawData[i] = converReceivedDataToU32(datagrama.data(), i);   // Преобразуем принятую датаграмму в массив u32

                if ( !backGroundNedeedClear ){              // Если нет необходимости чистить фон - работаем с собранным либо собираем
//                    /*--- Фон не собран -----------------------------------------*/
                    if ( !background_is_collected ){        // Если фон не собран

                        ui->progressBar->setRange(0, num_rotate);                                       // Заполняем прогресс бар
                        ui->progressBar->setValue(rotate);

                                    if(rotate < num_rotate) {                                           // Количество оборотов меньше чем необходимо для сбора фона. Продолжаем собирать фон
                                        for (uint32_t i = 0; i < sizeReadUdpData;  i++){                // Читаем принятый азимут и собираем максимальные значения для фона
                                            if ( rawData[i] > backGround[ currentAzimuth ][ i ] )       // Проверяем пр
                                                    backGround[ currentAzimuth ][ i ] = rawData[i];     // Приняли большее значение в азимуте чем
                                         }
                                    }else{
                                           background_is_collected = true;                              // Устанавливаем признак "Фон собран". Сбор фона закончен
                                           stateView ( ALL_DATA );                                      // После того как фон собран включаем отображение всех данных

                                           QString strDebugOutLW = "INFO: continue received data from port:" + ui->port_RawData->text();
                                           ADDITEM_DEBUG(strDebugOutLW)
                                    }
                    }
                    /*--- Фон собран --------------------------------------------*/
                    else{
                        for (uint32_t i = 0; i < sizeReadUdpData;  i++){
                           int32_t diffData = (int)(rawData[i] - backGround[ currentAzimuth ][ i ]);    // разницу принятого значения уроня сигнала и фона,  в азимуте,
                           diffData -= threshold;

                           if( diffData  > 0)           rawDataDiff [i] = diffData;                    // приводим к знаковому типу для обнуления отрцательных результотов вычета уроовня сигнала из фона
                           else                         rawDataDiff [i] =   0;
                        }
                    }
                    /*--- Вывод на график ---------------------------------------*/
                    if  ( oneAzimuthView ){                                                             // Если необходимо выводить на график один азимут
                        if( ui->valueOneAzimuth->value() == currentAzimuth)                             // Смотрим какой азимут отображать
                            drawAzimuth(rawData, rawDataDiff);                                          // Вывод на график
                        return;                                                                         // Выходим из обработчика. Далее отображать ничего не нужно
                    }

                    if( rangeAzimuthViewValue > 0 | rangeAzimuthViewValue < 0 ){
                        if ( rangeAzimuthViewValue > 0 ){
                            if( currentAzimuth < maxAzimuthViewValue  &&  currentAzimuth >= minAzimuthViewValue )   // Смотрим какой диапазон азимутов отображать
                                                                            drawAzimuth(rawData, rawDataDiff);      // Выводим на график по одному азимуту из диапазона

                        }
                        if ( rangeAzimuthViewValue < 0 ){
                            if( currentAzimuth < maxAzimuthViewValue  |  currentAzimuth >= minAzimuthViewValue )   // Смотрим какой диапазон азимутов отображать
                                                                            drawAzimuth(rawData, rawDataDiff);      // Выводим на график по одному азимуту из диапазона

                        }
                                                                                                                // Если необходимо отображать на графике диапазон азимутов
                    }
                    else                                                                                        // Если не указан диапазон или один азимут для вывода на график, то выводим все азимуты
                        drawAzimuth(rawData, rawDataDiff);                                                 // Отображаем ВСЕ азимуты
                }
                else{
                        ADDITEM_DEBUG("INFO: waiting for BackGround will clear...")
                        memset( (void *)backGround, 0 , sizeReadUdpData * maxNumAzimuth * sizeof(int) );        // Чистим собранный фон
                        ADDITEM_DEBUG("INFO: clear backGround is done")
                        backGroundNedeedClear = false;                                                          // Фон чистить не надо. Только что очистили
                        ADDITEM_DEBUG("INFO: waiting for BackGround will be collected...")
                }
           }
}
void MainWindow::readPendingDatagramsMavLink(){

    if(mSocketMavLink->hasPendingDatagrams()){

//        qDebug() << "<--Telemetry-->";

        QByteArray datagrama;
        datagrama.resize(mSocketMavLink->pendingDatagramSize());
        mSocketMavLink->readDatagram( datagrama.data(), datagrama.size() );

        parser((char *)datagrama.data(), datagrama.size());
    }
}
void MainWindow::slotTimerAlarm()
{
    if( cnt == 100)   cnt = 0;
    else              cnt = cnt + 1;
}
void MainWindow::on_pushButton_25_clicked()
{
//    static char bufToSend[sizeReadUdpData] = {0,};
//    const size_t sizeBuf = sizeReadUdpData;
//    for(uint32_t i = 1; i < sizeReadUdpData ; i++){
//        bufToSend[i] = i;
//    }

//    QByteArray datagrama = ("");

//    for(size_t i = 0; i < sizeBuf; i++)
//        datagrama.append('0' + i);

//    mSocket->writeDatagram(datagrama, QHostAddress::Broadcast,
//                           ui->port_server->value() );
}
void MainWindow::on_pushButton_11_clicked()
{
    replayStart = true;
}
void MainWindow::on_button_singleAzimuthView_clicked()
{    
    oneAzimuthView = false;

    rangeAzimuthViewValue = ui->valueAzimuth_2->value() - ui->valueAzimuth->value();
    if ( rangeAzimuthViewValue > 0){
            ADDITEM_DEBUG( "WARNING: drawed not all azimuth!" )
    }
    else{
            ADDITEM_DEBUG( "INFO: drawed ALL azimuth " )
    }
    maxAzimuthViewValue   = ui->valueAzimuth_2->value();
    minAzimuthViewValue   = ui->valueAzimuth->value();
}
void MainWindow::on_pushButton_allDataView_clicked()
{
    ADDITEM_DEBUG( "INFO: all data view state is active " )

    stateView ( ALL_DATA );
}
void MainWindow::on_pushButton_SetXY_clicked()
{
    xAxis.setRange(ui->valueXmin->value(), ui->valueXmax->value());
    yAxis.setRange(ui->valueYmin->value(), ui->valueYmax->value());
}
void MainWindow::on_pushButton_RawDataView_clicked()
{
    ADDITEM_DEBUG("INFO: rawData view state is active")
    ADDITEM_DEBUG("INFO: background_is_collected = true")

    background_is_collected = true;     // Включаем признак "ФОН СОБРАН". При установленном признаке
    stateView ( RAW_DATA );
}
void MainWindow::on_pushButton_ClearChar_clicked()
{
    ADDITEM_DEBUG( "INFO: clear all chart data " )

    seriesRawData.clear();      // Очищаем график при переключении режима отображениея (с роу даты на разностную, роу замирает последний отображенный азимут)
    seriesRawDataSub.clear();
}
void MainWindow::on_pushButton_DiffDataView_clicked()
{
    ADDITEM_DEBUG("INFO: Difference data view state is active")

    if ( !background_is_collected ){
            ADDITEM_DEBUG("Err: Background is missing")
    }

    stateView( DIFF_DATA );
}
void MainWindow::on_pushButton_StartBGCollecting_clicked()
{
    backGroundNedeedClear = true;

//    for (size_t i = 0; i < maxNumAzimuth; i++)
//        for (size_t j = 0; j < sizeReadUdpData; i++)
//                    backGround[i][j] = 0;

//    backGroundNedeedClear = false;
    rotate = 0;
    background_is_collected = false;
    RawDataVievIsActive = false;
    enableStartBackGrounCollection = true;
    num_rotate = ui->num_rotate->value();
}
void MainWindow::on_pushButton_AllAzimutView_clicked()
{
    ADDITEM_DEBUG( "INFO: drawed ALL azimuth " )

    oneAzimuthView = false;
    rangeAzimuthViewValue = 0;
    minAzimuthViewValue = 0;
    maxAzimuthViewValue = 0;
}
void MainWindow::on_pushButton_RawData_clicked()
{
//    mSocket->connectToHost("192.168.10.195", 15552);
//    if (mSocket->waitForConnected(1000))
//        qDebug("Connected!");

//    mSocket->connectToHost("192.168.10.195", 15552);
//    if(mSocket->waitForConnected(30000)) {
//        qDebug()  << "-- Connected in" ;
//    } else {
//        qDebug()  << "-- NOT Connected in" ;
//    }

//   qDebug() << "LocalAdress:" << mSocket->localAddress();



   background_is_collected = true;     // Включаем признак "ФОН СОБРАН". При установленном признаке
   stateView ( RAW_DATA );
}
void MainWindow::on_pushButton_19_clicked()
{
    mSocketMavLink->bind(ui->port_mavlink->value(), QUdpSocket::ShareAddress);
}
void MainWindow::on_pushButton_SendHB_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_heartbeat_t packet1;

    ADDITEM_DEBUG( "INFO: send HB" )

    mavlink_msg_heartbeat_encode(SYS_ID, COMPONENT_ID, &msgTx, &packet1);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val,
//                                              QHostAddress(ui->lineEdit_IP_MavLink->text()),
                                   QHostAddress("192.168.5.255"),
                                              15553);
}
void MainWindow::on_pushButton_EnPamp_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: pamp enable" )

    param_set.param_value = 1; /*<  Onboard parameter value*/
    param_set.param_index = 1; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    // param_set->param_id[16] = "PAMP"; /*<  Onboard parameter id, terminated by NULL if the length is less than 16 human-readable chars and WITHOUT null termination (NULL) byte if the length is exactly 16 chars - applications have to provide 16+1 bytes storage if the ID is stored as string*/
    param_set.param_type = 0;

    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);

    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val,  QHostAddress(ui->lineEdit_IP_MavLink->text()),  ui->port_mavlink->value());
}
void MainWindow::on_pushButton_DisPamp_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: pamp disenable" )

    param_set.param_value = 0; /*<  Onboard parameter value*/
    param_set.param_index = 1; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    // param_set->param_id[16] = "PAMP"; /*<  Onboard parameter id, terminated by NULL if the length is less than 16 human-readable chars and WITHOUT null termination (NULL) byte if the length is exactly 16 chars - applications have to provide 16+1 bytes storage if the ID is stored as string*/
    param_set.param_type = 0;

    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);

    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val,  QHostAddress(ui->lineEdit_IP_MavLink->text()),  ui->port_mavlink->value());
}
void MainWindow::on_pushButton_atten_0_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: atten 0" )

    param_set.param_value = 0; /*<  Onboard parameter value*/
    param_set.param_index = 0; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
     mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}
void MainWindow::on_pushButton_atten_10_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: atten 10" )

    param_set.param_value = 10; /*<  Onboard parameter value*/
    param_set.param_index = 0; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
     mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());
}
void MainWindow::on_pushButton_atten_20_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: atten 20" )

    param_set.param_value = 20; /*<  Onboard parameter value*/
    param_set.param_index = 0; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}
void MainWindow::on_pushButton_atten_30_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: atten 30" )

    param_set.param_value = 30; /*<  Onboard parameter value*/
    param_set.param_index = 0; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}
void MainWindow::on_pushButton_atten_40_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: atten 40" )

    param_set.param_value = 40; /*<  Onboard parameter value*/
    param_set.param_index = 0; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}
void MainWindow::on_pushButton_atten_50_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: atten 50" )

    param_set.param_value = 50; /*<  Onboard parameter value*/
    param_set.param_index = 0; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}
void MainWindow::on_pushButton_atten_60_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: atten 60" )

    param_set.param_value = 60; /*<  Onboard parameter value*/
    param_set.param_index = 0; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
     mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}
void MainWindow::on_pushButton_rxamp_1_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: rx amp 1" )

    param_set.param_value = 1; /*<  Onboard parameter value*/
    param_set.param_index = 2; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
     mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}
void MainWindow::on_pushButton_rxamp_2_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: rx amp 2" )

    param_set.param_value = 2; /*<  Onboard parameter value*/
    param_set.param_index = 2; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());
}
void MainWindow::on_pushButton_rxamp_3_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: rx amp 3" )

    param_set.param_value = 3; /*<  Onboard parameter value*/
    param_set.param_index = 2; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}
void MainWindow::on_pushButton_rxamp_4_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: rx amp 4" )

    param_set.param_value = 4; /*<  Onboard parameter value*/
    param_set.param_index = 2; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}
void MainWindow::on_button_OneAzimuth_clicked()
{
    ADDITEM_DEBUG( "WARNING: drawed not all azimuth!" )
    oneAzimuthView = true;
}
void MainWindow::on_button_threshhold_clicked()
{
        threshold = ui->value_threshold->value();
}
void MainWindow::on_button_numAzimuth_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    QString strDebugOutLW = "INFO: numAzimuth: " + ui->value_numAzimuth->text();
    ADDITEM_DEBUG(strDebugOutLW)

    param_set.param_value = ui->value_numAzimuth->value(); /*<  Onboard parameter value*/
    param_set.param_index = 3; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    // param_set->param_id[16] = "PAMP"; /*<  Onboard parameter id, terminated by NULL if the length is less than 16 human-readable chars and WITHOUT null termination (NULL) byte if the length is exactly 16 chars - applications have to provide 16+1 bytes storage if the ID is stored as string*/
    param_set.param_type = 0;

    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);

    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val,  QHostAddress(ui->lineEdit_IP_MavLink->text()),  ui->port_mavlink->value());

}
void MainWindow::on_pushButton_XYFull_clicked()
{
    xAxis.setRange(-1000, 10000);
    yAxis.setRange(0, ui->valueYmax->value());
}
void MainWindow::on_pushButton_XYLeftSide_clicked()
{
    xAxis.setRange(-250, 250);
    yAxis.setRange(0, ui->valueYmax->value());
}
void MainWindow::on_pushButton_XYRigthSide_clicked()
{
    xAxis.setRange(7950, 8450);
    yAxis.setRange(0, ui->valueYmax->value());
}
void MainWindow::on_pushButton_7_clicked()
{

}
void MainWindow::on_pushButton_AttenSet_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: atten" )

//    param_set.param_value = ui->valueAtten->value(); /*<  Onboard parameter value*/
    param_set.param_index = 0; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());
}
void MainWindow::on_button_ShiftValueQM_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: shiftValQM" )

    param_set.param_value = ui->value_ShiftValQM->value(); /*<  Onboard parameter value*/
    param_set.param_index = 4; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());
}
void MainWindow::on_lineEdit_IP_MavLink_cursorPositionChanged(int arg1, int arg2)
{

}
void MainWindow::on_pushButton_RunMotor_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: Run Motor" )

    param_set.param_value = 1; /*<  Onboard parameter value*/
    param_set.param_index = 6; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());
}
void MainWindow::on_pushButton_StopMotor_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: Stop Motor" )

    param_set.param_value = 0; /*<  Onboard parameter value*/
    param_set.param_index = 6; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());
}
void MainWindow::on_button_shiftDCO_clicked()
{
//    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
//    mavlink_message_t 	msgTx;
//    mavlink_param_set_t param_set;

//    ADDITEM_DEBUG( "INFO: shiftValQM" )

//    param_set.param_value = ui->value_ShiftDCO->value(); /*<  Onboard parameter value*/
//    param_set.param_index = 7; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
//    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
//    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
//    param_set.param_type = 0;
//    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
//    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
//    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());
}
//void MainWindow::on_pushButton_GetCompassAzimuth_clicked()
//{
////    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
////    mavlink_message_t 	msgTx;
////    mavlink_param_set_t param_set;

////    ADDITEM_DEBUG( "INFO: Run Motor" )

////    param_set.param_value = 1; /*<  Onboard parameter value*/
////    param_set.param_index = 6; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
////    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
////    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
////    param_set.param_type = 0;
////    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
////    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
////    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

//    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
//    mavlink_message_t 	msgTx;
//    mavlink_command_int_t command_int;

//    ADDITEM_DEBUG( "INFO: GetTelemetry" )

//    command_int.param1 = 9; /*<  Onboard parameter value*/
//    command_int.command = 0; // !!!!! Get Compas Azimuth Custom Command
//    command_int.target_system = SYS_ID;
//    mavlink_msg_command_int_encode(SYS_ID, COMPONENT_ID, &msgTx, &command_int);
//    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
//    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

////    while(!CompassValueIsRead){}

//    int i = compassValueAzimuth;
//    QString s = QString::number(i);

//    ADDITEM_DEBUG(s)
//    CompassValueIsRead = false;

//    /*******************************************************************/
//    QString strtempFPGA = QString::number(tempFPGA);
//    QString strtempLDO = QString::number(tempLDO);
//    QString strtempADPart = QString::number(tempADPart);

//    ADDITEM_DEBUG("TempFPGA: " + strtempFPGA + "    TempLDO: " + strtempLDO + "    TempADPart: " + strtempADPart)
//    /*******************************************************************/
//    uint64_t gpsRaw = GPSCoordinate;

//    double lon =  ( (double)(gpsRaw & 0xFFFFFFFF) / 10000000);
//    double lat =  ( (double)( (gpsRaw & 0xFFFFFFFF00000000) >> 32) / 10000000);

//    QString strLat = QString::number(lat);
//    QString strLon = QString::number(lon);

//    ADDITEM_DEBUG("lat: " + strLat)
//    ADDITEM_DEBUG("lon: " + strLon)
//    GPSValueIsRead = false;
//}
//void MainWindow::on_pushButton_GetGPSCoordinate_clicked()
//{
//    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
//    mavlink_message_t 	msgTx;
//    mavlink_command_int_t command_int;

//    ADDITEM_DEBUG( "INFO: GetGPSCoordinate" )

//    command_int.param1 = 10; /*<  Onboard parameter value*/
//    command_int.command = 0; // !!!!! Get Compas Azimuth Custom Command
//    command_int.target_system = SYS_ID;
//    mavlink_msg_command_int_encode(SYS_ID, COMPONENT_ID, &msgTx, &command_int);
//    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
//    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

////    while(!CompassValueIsRead){}

////    uint64_t i = GPSCoordinate;

////    double lon =  ( (double)(i & 0xFFFFFFFF) / 10000000);
////    double lat =  ( (double)( (i & 0xFFFFFFFF00000000) >> 32) / 10000000);

////    QString strLat = QString::number(lat);
////    QString strLon = QString::number(lon);

////    ADDITEM_DEBUG("lat: " + strLat)
////    ADDITEM_DEBUG("lon: " + strLon)
////    GPSValueIsRead = false;
//}
//void MainWindow::on_pushButton_GetTemp_clicked()
//{
////    QString strtempFPGA = QString::number(tempFPGA);
////    QString strtempLDO = QString::number(tempLDO);
////    QString strtempADPart = QString::number(tempADPart);

////    ADDITEM_DEBUG("TempFPGA: " + strtempFPGA + "    TempLDO: " + strtempLDO + "    TempADPart: " + strtempADPart)
//}





void MainWindow::on_pushButton_GetTelemetryMRLS2D_clicked()
{
    //    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    //    mavlink_message_t 	msgTx;
    //    mavlink_param_set_t param_set;

    //    ADDITEM_DEBUG( "INFO: Run Motor" )

    //    param_set.param_value = 1; /*<  Onboard parameter value*/
    //    param_set.param_index = 6; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    //    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    //    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    //    param_set.param_type = 0;
    //    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    //    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    //    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        mavlink_message_t 	msgTx;
        mavlink_command_int_t command_int;

        ADDITEM_DEBUG( "INFO: <<---GetTelemetry--->>" )

        command_int.param1 = 9; /*<  Onboard parameter value*/
        command_int.command = 0; // !!!!! Get Compas Azimuth Custom Command
        command_int.target_system = SYS_ID;
        mavlink_msg_command_int_encode(SYS_ID, COMPONENT_ID, &msgTx, &command_int);
        uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
        mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

    //    while(!CompassValueIsRead){}

        int i = compassValueAzimuth;
        QString s = QString::number(i);

        ADDITEM_DEBUG("INFO: CompassAzimuth: " + s)
        CompassValueIsRead = false;

        /*******************************************************************/
        QString strtempFPGA = QString::number(tempFPGA);
        QString strtempLDO = QString::number(tempLDO);
        QString strtempADPart = QString::number(tempADPart);

        ADDITEM_DEBUG("INFO: TempFPGA: " + strtempFPGA + "    TempLDO: " + strtempLDO + "    TempADPart: " + strtempADPart)
        /*******************************************************************/
        uint64_t gpsRaw = GPSCoordinate;

        double lon =  ( (double)(gpsRaw & 0xFFFFFFFF) / 10000000);
        double lat =  ( (double)( (gpsRaw & 0xFFFFFFFF00000000) >> 32) / 10000000);

        QString strLat = QString::number(lat);
        QString strLon = QString::number(lon);

        ADDITEM_DEBUG("INFO: lat: " + strLat + " lon: " + strLon)
//        ADDITEM_DEBUG("lon: " + strLon)
        GPSValueIsRead = false;
}








void MainWindow::on_pushButton_MotorSpeed1_25sec_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: motorSpeed 1.25sec" )

            param_set.param_value = 0; /*<  Onboard parameter value*/
            param_set.param_index = 8; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
            param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
            param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
            param_set.param_type = 0;
            mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
            uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
            mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}


void MainWindow::on_pushButton_MotorSpeed2_5sec_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: motorSpeed 2.5sec" )

            param_set.param_value = 1; /*<  Onboard parameter value*/
            param_set.param_index = 8; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
            param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
            param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
            param_set.param_type = 0;
            mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
            uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
            mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}


void MainWindow::on_pushButton_MotorSpeed5sec_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: motorSpeed 5sec" )

    param_set.param_value = 2; /*<  Onboard parameter value*/
    param_set.param_index = 8; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}


void MainWindow::on_pushButton_Set120MHz_HMC769_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_command_int_t command_int;

    ADDITEM_DEBUG( "INFO: switchStateTransmitTelemetry" )

    command_int.param1 = 12; /*<  Onboard parameter value*/
    command_int.command = 0; /*<  Onboard parameter value*/
    command_int.param2 = 0; // !!!!! Get Compas Azimuth Custom Command
    command_int.target_system = SYS_ID;
    mavlink_msg_command_int_encode(SYS_ID, COMPONENT_ID, &msgTx, &command_int);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}

void MainWindow::on_pushButton_SetCustomFreqBand_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_command_int_t command_int;


        QString str = "INFO: set Custom FrequencyBand " ;//+ QString::number(ui->value_Fstart->value() )  + QString::number(ui->value_Fstop->value() ) ;
//                                                        + QString::number(ui->value_Fstop->value() ) +
//                                                        + QString::number(ui->value_Tramp->value() );

    ADDITEM_DEBUG( str )

    command_int.param1 = 12; /*<  Onboard parameter value*/
    command_int.command = 0; /*<  Onboard parameter value*/
    command_int.param2 = 1; // !!!!! Get Compas Azimuth Custom Command
    command_int.param3 = ui->value_Fstart->value();
    command_int.param4 = ui->value_Fstop->value();
    command_int.param5 = ui->value_Tramp->value();
    command_int.target_system = SYS_ID;
    mavlink_msg_command_int_encode(SYS_ID, COMPONENT_ID, &msgTx, &command_int);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}


void MainWindow::on_pushButton_SetAtten_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    param_set.param_value = ( ui->value_Atten->value() * 2) ; /*<  Onboard parameter value*/

    if ( ui->value_Atten->value() > 31 )
    {
        QString str = "ERROR: SetAtenVal is not avalible for valid range = " + QString::number( ui->value_Atten->value()) ;
        ADDITEM_DEBUG( str )
        return;
    }else{
        QString str = "INFO: SetAtenVal = " + QString::number( ui->value_Atten->value()) ;
        ADDITEM_DEBUG( str )
    }

    param_set.param_index = 0; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
    param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
    param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
    param_set.param_type = 0;
    mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
     mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());
}


void MainWindow::on_pushButton_EnablePrintTelemetry_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_command_int_t command_int;

    ADDITEM_DEBUG( "INFO: switchStateTransmitTelemetry" )

    command_int.param1 = 11; /*<  Onboard parameter value*/
    command_int.command = 0; // !!!!! Get Compas Azimuth Custom Command
    command_int.target_system = SYS_ID;
    mavlink_msg_command_int_encode(SYS_ID, COMPONENT_ID, &msgTx, &command_int);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}


void MainWindow::on_pushButton_TrigerSwitch_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_command_int_t command_int;


        QString str = "INFO: Switch Triger" ;//+ QString::number(ui->value_Fstart->value() )  + QString::number(ui->value_Fstop->value() ) ;
//                                                        + QString::number(ui->value_Fstop->value() ) +
//                                                        + QString::number(ui->value_Tramp->value() );

    ADDITEM_DEBUG( str )

    command_int.param1 = 13; /*<  Onboard parameter value*/
    command_int.command = 0; /*<  Onboard parameter value*/
    command_int.param2 = 1; // !!!!! Get Compas Azimuth Custom Command
    command_int.param3 = ui->value_Fstart->value();
    command_int.param4 = ui->value_Fstop->value();
    command_int.param5 = ui->value_Tramp->value();
    command_int.target_system = SYS_ID;
    mavlink_msg_command_int_encode(SYS_ID, COMPONENT_ID, &msgTx, &command_int);
    uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
    mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}


void MainWindow::on_button_ShiftDCO_clicked()
{

        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        mavlink_message_t 	msgTx;
        mavlink_command_int_t command_int;

        ADDITEM_DEBUG( "INFO: reservData" )

        command_int.param1 = 14; /*<  Onboard parameter value*/
        command_int.command = 0; // !!!!! Get Compas Azimuth Custom Command
        command_int.target_system = SYS_ID;
        mavlink_msg_command_int_encode(SYS_ID, COMPONENT_ID, &msgTx, &command_int);
        uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
        mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}


void MainWindow::on_pushButton_AGCEnable_clicked()
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    mavlink_message_t 	msgTx;
    mavlink_param_set_t param_set;

    ADDITEM_DEBUG( "INFO: motorSpeed 1.25sec" )

            param_set.param_value = 0; /*<  Onboard parameter value*/
            param_set.param_index = 9; /*<  Parameter index. Send 0 to use the param ID field as identifier (else the param id will be ignored)*/
            param_set.target_system = TARGET_SYSTEM; /*<  System ID*/
            param_set.target_component = TARGET_COMPONENT; /*<  Component ID*/
            param_set.param_type = 0;
            mavlink_msg_param_set_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_set);
            uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)buffer, &msgTx);
            mSocketMavLink->writeDatagram( ( const char * )buffer , len_val, QHostAddress(ui->lineEdit_IP_MavLink->text()), ui->port_mavlink->value());

}

