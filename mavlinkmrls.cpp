#include "mavlinkmrls.h"

mavlink_message_t rxMessage;
mavlink_status_t rxStatus;
mavlink_heartbeat_t heartbeat;

bool CompassValueIsRead = false;
bool enable_transmit_data_to_PC = true;
uint32_t compassValueAzimuth = 0;
uint64_t GPSCoordinate = 0;
bool GPSValueIsRead = false;
extern float tempFPGA;
extern float tempLDO;
extern float tempADPart;

int handler_COMMAND_INT(mavlink_message_t* message){

    mavlink_command_int_t command_int;
    uint16_t len_cmd_ack;
//	xil_printf("INFO: handler_COMMAND_INT\r\n");

    mavlink_msg_command_int_decode(message, &command_int);
    if(command_int.target_system != CURRENT_SYSID)
            return -1;

        switch (command_int.command)    // Смотрим принятую команду
        {
            case CUSTOM:	 switch (command_int.param1)    // Смотрим принятую команду
                            {
//								case 0: 	xil_printf("CMD: Enable AMP\n\r");
                                    break;
//								case 1:		xil_printf("CMD: Set ATTEN\n\r");
//											xil_printf("CMD: ATTEN:%d\n\r", command_int.param2);

//											HMC769_setAtten(command_int.param2);
                                    // Приняли команду включить усилительь
                                    break;
//								case 2:		xil_printf("CMD: Enable TRIG\n\r");		// Приняли команду включить усилительь
//											xil_printf("CMD: VAL_TRIG:%d\n\r", command_int.param2);
                                    break;
//								case 3:		xil_printf("CMD: AMP\n\r");		// Приняли команду включить усилительь
//											xil_printf("CMD: AMP_VAL:%d\n\r", command_int.param2);
//											if(command_int.param2 <= 3)
//												configReciever(RECIEVER_NUM_AMP, ON, command_int.param2);
                                    break;
                                case 4:		//xil_printf("CMD: frameSizeML\n\r");		// Приняли команду включить усилительь
                                    break;
                                case 5: 	//xil_printf("CMD: Set delayset\n\r");
                                            //xil_printf("CMD: delayset:%d\n\r", command_int.param2);
//											AD9508_writeData(0x17, command_int.param2);
                                    break;
                                case 6: 	//xil_printf("CMD: Get version\n\r");
                                            //xil_printf("CMD: version:%d\n\r", MRLS_VERSION);
                                    break;
                                case 7: 	//xil_printf("CMD: Enable TX data\n\r");
                                            if(command_int.param2)
                                                enable_transmit_data_to_PC = true;
                                            else
                                                enable_transmit_data_to_PC = false;
                                break;
                                case 8: 	//xil_printf("CMD: Enable TX data\n\r");
//											if(command_int.param2)
//												enable_transmit_RAWdata_to_PC = true;
//											else
//												enable_transmit_RAWdata_to_PC = false;
                                break;
                                case 9: break;
                                default : break;
                            }






            break;
            case REBOOT:				break;			//send_cmdACK_mavLink();
//										mb_reset();
                                        break;
            case RESET_PARAM:	    	/*------------------*/	break;
            case BT_CMD_TYPE_ENUM_END:  /*------------------*/	break;
//            default:   					sendError(0);
            break;
        }
    return 1;
}
int handler_PARAM_GET(mavlink_message_t* message){

//	mavlink_param_value_t param_value;
//	char bufTx[128];
//	xil_printf("INFO: handler_PARAM_GET\r\n");

//	mavlink_msg_param_value_decode(message, &param_value);
//	// Проверяем нам ли это сообщение
////	if (param_value.target_system != CURRENT_SYSID)
////		return -1;

////	param_value.param_value = param_set.param_value;
////	param_value.param_index = param_set.param_index;


//	uint64_t data;
//	getParam(&param_value, (uint64_t *)&data);
//	xil_printf("INFO:%d\r\n", data);
//	mavlink_message_t 	msgTx;

//		mavlink_msg_param_value_encode(SYS_ID, COMPONENT_ID, &msgTx, &(params[(param_value.param_index) - 1].param));
//		uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
//		my_udp_send(bufTx, len_val, &ip_addr_broadcast);

        return 1;
}
int handler_PARAM_VALUE(mavlink_message_t* message){

    mavlink_param_value_t param_value;
//    char bufTx[128];

    mavlink_msg_param_value_decode(message, &param_value);

    switch(param_value.param_count){

        case 3:
            compassValueAzimuth = param_value.param_value;
            qDebug() << "INFO: Compass azimuth = " << param_value.param_value ;
            CompassValueIsRead = true;
        break;
        case 4:
            GPSCoordinate = param_value.param_value;

            uint64_t gpsRaw = GPSCoordinate;

            double lon =  ( (double)(gpsRaw & 0xFFFFFFFF) / 10000000);
            double lat =  ( (double)( (gpsRaw & 0xFFFFFFFF00000000) >> 32) / 10000000);

            qDebug() << "INFO: lon" << lon ;
            qDebug() << "INFO: lat" << lat ;
            GPSValueIsRead = true;
        break;
    }
    return 0;
}
int handler_PARAM_SET(mavlink_message_t* message){

    static mavlink_param_set_t param_set;
    static mavlink_param_value_t param_value;

//	xil_printf("INFO: handler_PARAM_SET\r\n");

    mavlink_msg_param_set_decode(message, &param_set);
    // Проверяем нам ли это сообщение
    if (param_set.target_system != CURRENT_SYSID)
        return -1;

    switch (param_set.param_index)    // Смотрим принятую команду
                {
                    case 0: 	//mrls_print("INFO: Atten\n\r");
                                //HMC769_setAtten( param_set.param_value );
                                break;

                    case 1: 	//mrls_print("INFO: Pamp\n\r");
                                //if( param_set.param_value)	HMC769_PWR_EN( EN_SPI_CEN |EN_PAMP  | EN_ROW_PLL );
                                //else						HMC769_PWR_EN( EN_SPI_CEN |DIS_PAMP | EN_ROW_PLL );
                                break;

                    case 2: 	//mrls_print("INFO: RXamp\n\r");
                                //AD9650_ADC_SetSwitch_AMP(param_set.param_value);
                                break;

                    default : break;
                }


//	param_value.param_value = param_set.param_value;
//	param_value.param_index = param_set.param_index;
//	param_value.param_id    = param_set.param_id;




//	setParam(&param_value);
        return 1;
}
int handler_TEMP_RAW(mavlink_message_t* message)
{
    mavlink_temp_raw_t temp_raw;
    mavlink_msg_temp_raw_decode(message, &temp_raw);

    tempFPGA =      (float)(temp_raw.time  & 0xFFFF) / 100 ;
    tempLDO =       (float)((temp_raw.time & 0xFFFF0000) >> 16 ) / 100 ;
    tempADPart =  ( (float)((uint8_t)temp_raw.reserv_1 | ((uint8_t)temp_raw.reserv_2 << 8)) / 100 ) ;

    qDebug() << "INFO: tempFPGA"   << tempFPGA ;
    qDebug() << "INFO: tempLDO"    << tempLDO ;
    qDebug() << "INFO: tempADPart" << tempADPart ;

    return 1;
}
void handler_heartbeat(mavlink_message_t* message){

    mavlink_msg_heartbeat_decode(message, &heartbeat);

//    xil_printf("handler_heartbeat ---%x,%x\r\n", heartbeat.state, heartbeat.version);
}
int handler_PARAM_REQUEST_LIST(mavlink_message_t* message)
{
    mavlink_param_request_list_t param_request_list;
    // Декодир

//	xil_printf("INFO: handler_PARAM_REQUEST_LIST\r\n");

    mavlink_msg_param_request_list_decode(message, &param_request_list);
    // Проверяем нам ли это сообщение
    if (param_request_list.target_system != CURRENT_SYSID)
        return -1;
    return 1;
}

void handler(mavlink_message_t* message){

    switch (message->msgid) {
    case MAVLINK_MSG_ID_HEARTBEAT:
        handler_heartbeat(message);
//        mrls_print("received HB\r\n");
        break;

    case MAVLINK_MSG_ID_PARAM_GET:
        handler_PARAM_GET(message);             	break;

    case MAVLINK_MSG_ID_PARAM_SET:
        handler_PARAM_SET(message);				// RXAMP ATTEN PAMP
        break;
    case MAVLINK_MSG_ID_COMMAND_INT:
        handler_COMMAND_INT(message);
        break;

    case MAVLINK_MSG_ID_PARAM_VALUE:
        handler_PARAM_VALUE(message);
        break;

    case MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
        handler_PARAM_REQUEST_LIST(message);
        break;

    case MAVLINK_MSG_ID_TEMP_RAW:
        handler_TEMP_RAW(message);
        break;
//    case MAVLINK_MSG_ID_PARAM_REQUEST_GROUP:
//        handler_param_set(message);		        break;

//    case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
//        handler_param_REQUEST_READ(message);        break;

    default:

        break;
    }
}
void parser(const char* buffer, size_t len)
{
    mavlink_message_t message;
    mavlink_status_t status;
    for (size_t pos = 0; pos < len; pos++) {
        uint8_t res = mavlink_frame_char_buffer(&rxMessage, &rxStatus, (uint8_t)buffer[pos], &message, &status);
        switch (res) {

            case MAVLINK_FRAMING_INCOMPLETE:
            break;
                case MAVLINK_FRAMING_OK:
                        handler(&message);
                break;
                    case MAVLINK_FRAMING_BAD_CRC:
                    break;
                        case MAVLINK_FRAMING_BAD_SIGNATURE:
                        break;
        }
    }
}
