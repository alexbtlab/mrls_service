QT       += core gui charts network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    mavlink/mavlink_parser/handlers.c \
    mavlink/mavlink_parser/parser.c \
    mavlinkmrls.cpp

HEADERS += \
    mainwindow.h \
    mavlink/btlink/src/BTLink/BTLink.h \
    mavlink/btlink/src/BTLink/mavlink.h \
    mavlink/btlink/src/BTLink/mavlink_msg_amplifier.h \
    mavlink/btlink/src/BTLink/mavlink_msg_auth_key.h \
    mavlink/btlink/src/BTLink/mavlink_msg_cmd.h \
    mavlink/btlink/src/BTLink/mavlink_msg_cmd_ack.h \
    mavlink/btlink/src/BTLink/mavlink_msg_command_ack.h \
    mavlink/btlink/src/BTLink/mavlink_msg_command_cancel.h \
    mavlink/btlink/src/BTLink/mavlink_msg_command_int.h \
    mavlink/btlink/src/BTLink/mavlink_msg_command_long.h \
    mavlink/btlink/src/BTLink/mavlink_msg_debug.h \
    mavlink/btlink/src/BTLink/mavlink_msg_drive.h \
    mavlink/btlink/src/BTLink/mavlink_msg_error.h \
    mavlink/btlink/src/BTLink/mavlink_msg_gps_raw.h \
    mavlink/btlink/src/BTLink/mavlink_msg_heartbeat.h \
    mavlink/btlink/src/BTLink/mavlink_msg_param_get.h \
    mavlink/btlink/src/BTLink/mavlink_msg_param_request_group.h \
    mavlink/btlink/src/BTLink/mavlink_msg_param_request_list.h \
    mavlink/btlink/src/BTLink/mavlink_msg_param_request_read.h \
    mavlink/btlink/src/BTLink/mavlink_msg_param_set.h \
    mavlink/btlink/src/BTLink/mavlink_msg_param_set_ack.h \
    mavlink/btlink/src/BTLink/mavlink_msg_param_value.h \
    mavlink/btlink/src/BTLink/mavlink_msg_ping.h \
    mavlink/btlink/src/BTLink/mavlink_msg_power.h \
    mavlink/btlink/src/BTLink/mavlink_msg_raw_data.h \
    mavlink/btlink/src/BTLink/mavlink_msg_target_data.h \
    mavlink/btlink/src/BTLink/mavlink_msg_target_id_data.h \
    mavlink/btlink/src/BTLink/mavlink_msg_target_id_data_from_model.h \
    mavlink/btlink/src/BTLink/mavlink_msg_temp_raw.h \
    mavlink/btlink/src/BTLink/mavlink_msg_text.h \
    mavlink/btlink/src/BTLink/testsuite.h \
    mavlink/btlink/src/BTLink/version.h \
    mavlink/btlink/src/checksum.h \
    mavlink/btlink/src/mavlink_conversions.h \
    mavlink/btlink/src/mavlink_get_info.h \
    mavlink/btlink/src/mavlink_helpers.h \
    mavlink/btlink/src/mavlink_sha256.h \
    mavlink/btlink/src/mavlink_types.h \
    mavlink/btlink/src/protocol.h \
    mavlink/mavlink_parser/mavlink_parser.h \
    mavlinkmrls.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    mavlink/btlink/message_definitions/BTLink.xml

INCLUDEPATH += \
                mavlink/btlink/src/BTLink \
                mavlink/mavlink_parser

RESOURCES += \
    resources.qrc

