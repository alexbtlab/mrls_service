#ifndef MAVLINKMRLS_H
#define MAVLINKMRLS_H

#include "mavlink/mavlink_parser/mavlink_parser.h"
#include "mavlink/mavlink_parser/parser.c"
#include <QDebug>

int handler_COMMAND_INT(mavlink_message_t* message);
void parser(const char* buffer, size_t len);

#endif // MAVLINKMRLS_H
